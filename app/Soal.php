<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    //
    protected $fillable = ['kategori',
						'tahun',
						'soal',
						'pilihan_a',
						'pilihan_b',
						'pilihan_c',
						'pilihan_d',
						'penjelasan',
						'jawaban',
						'penjelasan_gambar'
					];
}
