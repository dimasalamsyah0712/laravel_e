<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('cekuserakses');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo $usr;
        return view('home');
    }

    public function edit(User $user){
        return view('profile', compact('user'));
    }

    public function update(Request $request, User $user){
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $Namagambar = "no_image.jpg";

        if($request->foto != null){
            $Namagambar = request()->foto->getClientOriginalName();
            request()->foto->move(public_path('images/uploads'), $Namagambar);
        }

        $user->name = $request->nama;
        $user->tempat_lahir = $request->tempat_lahir;
        $user->tgl_lahir = $request->tgl_lahir;
        $user->alamat = $request->alamat;
        $user->foto = $Namagambar;

        $user->save();

        return redirect('/home')
                      ->with('success', 'Simpan berhasil.');
    }


}
