<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Soal;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        echo "not found";
    }

    public function showquiz($soal, $tahun)
    {
        /*$email = Auth::user()->email;
        $status = Quiz::where('idUser', $email)->first();*/

        //print_r($status);

        $soals = DB::select("SELECT * from soals where kategori = '$soal' and tahun = '$tahun' ");
        return view('ujian', ['soals'=>$soals, 'kategori'=> $soal, 'tahun'=>$tahun]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function viewScore(){

        if (Auth::check()) {
            $email = Auth::user()->email;

            $soals = DB::select("SELECT b.kategori, b.tahun, count(b.id) as totalsoal, count(case when b.jawaban = a.jwbQuiz then 'benar' end) as score from quizzes a left join soals b on b.id = a.idSoal where a.idUser = '$email' group by b.tahun, b.kategori");

            return view('score', ['soals'=>$soals]);
        }else{
            return view('home');
        }

        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = array();
        $email = Auth::user()->email;
        $kategori = $request->kategori;
        $tahun = $request->tahun;

        /*$delete = DB::raw("DELETE a from quizzes a left join soals b on b.id = a.idSoal 
            where a.idUser = '$email' and b.tahun = '$tahun' and b.kategori = '$kategori' ")->get();
*/
        $delete = DB::table('quizzes')
                ->leftJoin('soals', 'soals.id', '=', 'quizzes.idSoal')
                ->where('quizzes.idUser', $email)
                ->where('soals.tahun', $tahun)
                ->where('soals.kategori', $kategori)
                ->delete();

        for($i = 1; $i <= $request->count; $i++){
            
            $s = "idSoal_".$i;
            $j = "jwb_".$i;

            $q = [
                'idSoal' => $request->$s,
                'idUser' => $email,
                'jwbQuiz' => $request->$j,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ];

            array_push($data, $q);
        }

        Quiz::insert($data); // Eloquent approach
        //DB::table('ujian')->insert($data); // Query Builder approach

        return redirect('/hasil/'.$kategori.'/'.$tahun)->with('success', 'Ujian berhasil tersimpan.');


    }

    public function resultQuiz($kategori, $tahun){

        $email = Auth::user()->email;

        $soals = DB::table('quizzes')
                ->leftJoin('soals', 'soals.id', '=', 'quizzes.idSoal')
                ->where('quizzes.idUser', $email)
                ->where('soals.tahun', $tahun)
                ->where('soals.kategori', $kategori)
                ->get();

        return view('result', ['soals'=>$soals, 'kategori'=>$kategori, 'tahun'=> $tahun]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz)
    {
        //
    }
}
