<?php

namespace App\Http\Controllers;

use App\Soal;
use Illuminate\Http\Request;

class SoalController extends Controller
{

    private $pelajaran;
    
    public function __construct(){
       $this->pelajaran = array(0 => 'B. Indonesia', 1 => 'Matematika', 2 => 'IPA'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($soal, $tahun)
    {
        //
        $soals = Soal::orderBy('id','desc')
        ->where([
                    ['kategori', '=', $soal],
                    ['tahun', '=', $tahun]
                ])
        ->paginate(10);
        $soals = Soal::orderBy('id','desc')->paginate(10);
        return view('soal', ['soals'=>$soals, 'soal'=> $soal, 'tahun'=>$tahun]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //print_r($this->pelajaran[0]);
        return view('soal_create', ['pelajaran' => $this->pelajaran] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $Namagambar = null;

        if($request->penjelasan_gambar != null){
            $Namagambar = request()->penjelasan_gambar->getClientOriginalName();
            request()->penjelasan_gambar->move(public_path('images/uploads'), $Namagambar);
        }

        $this->validate($request, [
            'pilihan_a' => 'required',
            'pilihan_b' => 'required',
            'pilihan_c' => 'required',
            'pilihan_d' => 'required',
            'penjelasan' => 'required'
        ]);

        $data = [
                'kategori' => $request->kategori,
                'tahun' => $request->tahun,
                'soal' => $request->soal,
                'pilihan_a' => $request->pilihan_a,
                'pilihan_b' => $request->pilihan_b,
                'pilihan_c' => $request->pilihan_c,
                'pilihan_d' => $request->pilihan_d,
                'penjelasan' => $request->penjelasan,
                'penjelasan_gambar' => $Namagambar,
                'jawaban' => $request->jawaban
            ];
        Soal::create($data);

        return redirect('soal/'.$request->kategori.'/'.$request->tahun)
                      ->with('success', 'Simpan berhasil.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function show(Soal $soal)
    {
        //
    }

    public function showSoal($soal, $tahun){
        $soals = Soal::orderBy('id','desc')
        ->where([
                    ['kategori', '=', $soal],
                    ['tahun', '=', $tahun]
                ])->paginate(10);
        return view('soal', ['soals'=>$soals, 'soal'=> $soal, 'tahun'=>$tahun]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function edit(Soal $soal)
    {
        //
        return view('soal_edit', compact('soal'), ['pelajaran' => $this->pelajaran]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Soal $soal)
    {
        //

        $Namagambar = null;

        if($request->penjelasan_gambar != null){
            $Namagambar = request()->penjelasan_gambar->getClientOriginalName();
            request()->penjelasan_gambar->move(public_path('images/uploads'), $Namagambar);
        }

        $this->validate($request, [
            'pilihan_a' => 'required',
            'pilihan_b' => 'required',
            'pilihan_c' => 'required',
            'pilihan_d' => 'required',
            'penjelasan' => 'required'
        ]);
        
        $soal->kategori = $request->kategori;
        $soal->tahun = $request->tahun;
        $soal->soal = $request->soal;
        $soal->pilihan_a = $request->pilihan_a;
        $soal->pilihan_b = $request->pilihan_b;
        $soal->pilihan_c = $request->pilihan_c;
        $soal->pilihan_d = $request->pilihan_d;
        $soal->penjelasan = $request->penjelasan;
        $soal->penjelasan_gambar = $Namagambar;
        $soal->jawaban = $request->jawaban;
        
        $soal->save();

        return redirect('soal/'.$request->kategori.'/'.$request->tahun)
                      ->with('success', 'Update berhasil.');

        /*return redirect()->route('soal.index')
                      ->with('success', 'Simpan berhasil.');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Soal $soal)
    {
        //
        $soal->delete();
        return redirect()->route('soal.index')
                      ->with('success', 'Hapus berhasil.');
    }
}
