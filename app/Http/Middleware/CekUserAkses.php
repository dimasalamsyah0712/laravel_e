<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CekUserAkses
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //$user = \App\User::where('email', $request->email)->first();

        /*if (Auth::user()->akses == 'admin') {
            return redirect( '/home'.Auth::user()->akses. );
        } else {
            return redirect( Auth::user()->akses.'/home'.Auth::user()->akses);
        }*/

        return redirect( '/home/'.Auth::user()->akses);


        //print_r($user);
        return $next($request);
    }
}
