<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Auth::check()) {
		return view('home');
	}else{
		return view('auth.login');
	}
    
});


Route::get('/tentang', function () {
    return view('tentang');
});

Auth::routes();

/*Route::get('/edit/{id}', 'HomeController@edit')->name('editnama');
Route::put('/edit/{id}', 'HomeController@ubahnama')->name('ubahnama');*/
Route::resource('/user', 'HomeController');

//Route::get('/tentang', 'HomeController@tentang')->name('tentang');


Route::resource('/soal', 'SoalController');
Route::get('/soal/{pel}/{thn}', 'SoalController@showSoal')->name('showsoal');

Route::resource('/ujian', 'QuizController');
Route::get('/ujian/{pel}/{thn}', 'QuizController@showquiz')->name('showquiz');
Route::get('/hasil/{pel}/{thn}', 'QuizController@resultQuiz')->name('resultquiz');

Route::get('/home', 'QuizController@viewScore')->name('score');

/*Route::get('edit/{id}', ['as' => 'user.edit', 'uses' => 'HomeController@editUser']);
Route::patch('edit/{id}', ['as' => 'user.update', 'uses' => 'HomeController@update']);
*/
/*Route::group(['namespace' => 'User', 'prefix' => 'user'], function(){
    Route::get('{nickname}/settings', ['as' => 'user.settings', 'uses' => 'HomeController@edit']);
    Route::get('{nickname}/profile', ['as' => 'user.profile', 'uses' => 'HomeController@edit']);
});*/
