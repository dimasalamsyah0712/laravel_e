@extends('layouts.app')

@section('content')

  <div id="templatemo_content_wrapper">
    <span class="top"></span><span class="bottom"></span>
    <div id="templatemo_content">
      <h2>What we do?</h2>
      <p>Pro Teal Template is a full-site (5 pages) <a href="http://www.templatemo.com" target="_parent">website template</a> provided by
        <a href="http://www.templatemo.com" target="_parent">templatemo.com</a> and you may edit and apply this template for your websites. Credits go to
        <a href="http://www.photovaco.com" target="_blank">Free Photos</a> for photos and <a href="http://www.smashingmagazine.com" target="_blank">SmashingMagazine.com</a> for icons used in this template. Validate
        <a  href="http://validator.w3.org/check?uri=referer" rel="nofollow">XHTML</a> <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow">CSS</a>.</p>
      <p>In ac libero urna. </p>
    </div> <!-- end -->

    <div id="templatemo_sidebar">

        <div id="sidebar_featured_project">
            <h3>Daftar</h3>
            <form method="POST" action="{{ route('register') }}">
              @csrf

              <label>Nama:</label><br>
              <input type="text" name="name" value="">

              <br><br>

              <label>Email:</label><br>
              <input type="email" name="email" value="">

              <br><br>

              <label>Password:</label><br>
              <input type="password" name="password" value="">
              <br><br>

              <label>Konfirmasi Password:</label><br>
              <input type="password" name="password_confirmation" value="">
              <br><br>

              <button type="submit" name="button">Daftar</button>&nbsp;<a href="{{ route('login') }}">Masuk</a>
            </form>

            <div class="cleaner"></div>
        </div>

        <div class="cleaner"></div>
    </div>

    <div class="cleaner"></div>

  </div>

@endsection
