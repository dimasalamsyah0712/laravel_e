@extends('layouts.app')

@section('content')

<div id="templatemo_wrapper">

      <div id="templatemo_content_wrapper">
        <span class="top"></span><span class="bottom"></span>
        <div id="templatemo_content">

          <h2>Home</h2>
          <p>Pro Teal Template is a full-site (5 pages) <a href="http://www.templatemo.com" target="_parent">website template</a> provided by
            <a href="http://www.templatemo.com" target="_parent">templatemo.com</a> and you may edit and apply this template for your websites. Credits go to
            <a href="http://www.photovaco.com" target="_blank">Free Photos</a> for photos and <a href="http://www.smashingmagazine.com" target="_blank">SmashingMagazine.com</a> for icons used in this template. Validate
            <a  href="http://validator.w3.org/check?uri=referer" rel="nofollow">XHTML</a> <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow">CSS</a>.</p>
          <p>In ac libero urna. </p>


        </div> <!-- end -->

        <div id="templatemo_sidebar">

            

            <div class="cleaner"></div>
        </div>

      <div class="cleaner"></div>

      </div>

  </div>

    <!-- Scripts -->
    <script src="{{ asset('vendor/js/jquery-1.3.1.min.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.ennui.contentslider.js') }}"></script>
    <script type="text/javascript">
        $(function() {
        $('#one').ContentSlider({
        width : '600px',
        height : '240px',
        speed : 600,
        easing : 'easeInOutQuart'
        });
        });
    </script>

@endsection
