@extends('layouts.app')

@section('content')

<style>
	h4, h6{
		color: white;
	}
</style>

<a href="{{url('soal/create')}}">Tambah Soal</a>

<br><br>

<h4>Soal : {{$soal}}</h4>
<h6>Tahun : {{$tahun}}</h6>

<table id="table">
	<tr>
		<th>No.</th>
		<th>Soal</th>
		<th>Pil. A</th>
		<th>Pil. B</th>
		<th>Pil. C</th>
		<th>Pil. D</th>
		<th>Pjlsn</th>
		<th>Pjlsn <br>Gambar</th>
		<th>Jwb</th>
		<th></th>
	</tr>
	
	@foreach($soals as $no=>$soal)
	<tr>
		<td>{{ ++$no }}</td>
		<td>{!! $soal->soal !!}</td>
		<td>{!! $soal->pilihan_a !!}</td>
		<td>{!! $soal->pilihan_b !!}</td>
		<td>{!! $soal->pilihan_c !!}</td>
		<td>{!! $soal->pilihan_d !!}</td>
		<td>{!! $soal->penjelasan !!}</td>
		<td>{{ $soal->penjelasan_gambar }}</td>
		<td>{{ $soal->jawaban }}</td>
		<td>
			<a href="{{ route('soal.edit', $soal->id) }}">Edit</a>
			<form id="form" onsubmit="return confirm('Yakin menghapus data?')" method="post" action="{{ route('soal.destroy', $soal->id) }}">
				@csrf
				@method('delete')
				{{-- <button type="submit">Hapus</button> --}}
				<a href="#" onclick="submit()">Hapus</a>
			</form>
			
		</td>
	</tr>
	@endforeach
	

</table>

<script>
	function submit(){
		$("#form").submit();
	}
</script>


@endsection
