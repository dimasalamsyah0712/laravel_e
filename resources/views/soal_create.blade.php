@extends('layouts.app')

@section('content')



<div id="templatemo_content_wrapper">
	<span class="top"></span><span class="bottom"></span>
	<div id="templatemo_content">

		@if($errors->all())
			<span style="color: red">Data tidak boleh kosong.</span>
		@endif

		<form method="post" action="{{ route('soal.store') }}" enctype="multipart/form-data">

			@csrf

			<label>Kategori</label><br>
			<select name="kategori">
				<option value="{{ $pelajaran[0] }}">B. Indonesia</option>
				<option value="{{ $pelajaran[1] }}">Matematika</option>
				<option value="{{ $pelajaran[2] }}">IPA</option>
			</select>

			<br><br>
			<label>Tahun Ajaran</label><br>
			<select name="tahun">
				<option value="2015">2015</option>
				<option value="2016">2016</option>
				<option value="2017">2017</option>
				<option value="2018">2018</option>
			</select>

			<br><br>
			<label>Soal</label>
			<textarea required="" class="" name="soal" id="editor1" rows="8" cols="80" style="margin-bottom: 15px"></textarea>

			<br>
			<label>Pilihan A</label>
			<textarea required="" class="" name="pilihan_a" id="editor2" rows="8" cols="80" style="margin-bottom: 15px"></textarea>

			<br>
			<label>Pilihan B</label>
			<textarea required="" class="" name="pilihan_b" id="editor3" rows="8" cols="80" style="margin-bottom: 15px"></textarea>

			<br>
			<label>Pilihan C</label>
			<textarea required="" class="" name="pilihan_c" id="editor4" rows="8" cols="80" style="margin-bottom: 15px"></textarea>

			<br>
			<label>Pilihan D</label>
			<textarea required="" class="" name="pilihan_d" id="editor5" rows="8" cols="80" style="margin-bottom: 15px"></textarea>

			<br>
			<label>Penjelasan</label>
			<textarea required="" class="" name="penjelasan" id="editor6" rows="8" cols="80" style="margin-bottom: 15px"></textarea>

			<br>
			<label>Jawaban</label><br>
			<select name="jawaban">
				<option value="A">A</option>
				<option value="B">B</option>
				<option value="C">C</option>
				<option value="D">D</option>
			</select>

			<br><br>
			<label>Penjelasan dengan gambar</label> <br>
		    <input type="file" class="" id="" name="penjelasan_gambar">

			<br><br>
			<input type="submit" name="submit" value="Simpan">
			<input type="reset" name="clear" value="Reset">

		</form>
	</div>

	<div id="templatemo_sidebar">
		<a href="{{ url('/soal/create') }}">Buat Soal</a><br>
	</div>

	<div class="cleaner"></div>
</div>

<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
  CKEDITOR.replace( 'editor1' );
  CKEDITOR.replace( 'editor2' );
  CKEDITOR.replace( 'editor3' );
  CKEDITOR.replace( 'editor4' );
  CKEDITOR.replace( 'editor5' );
  CKEDITOR.replace( 'editor6' );
</script>


@endsection
