@extends('layouts.app')

@section('content')



<div id="templatemo_content_wrapper">
	<span class="top"></span><span class="bottom"></span>
	<div id="templatemo_content">

		@if($errors->all())
			<span style="color: red">Data tidak boleh kosong.</span>
		@endif
		
		<form method="post" action="{{ route('soal.update', $soal->id) }}" enctype="multipart/form-data">

			@csrf
			@method('put')

			<label>Kategori</label><br>
			<select name="kategori">
				<option @if($soal->kategori == $pelajaran[0]) selected="selected" @endif value="{{$pelajaran[0]}}">Bahasa Indonesia</option>
				<option @if($soal->kategori == $pelajaran[1]) selected="selected" @endif value="{{$pelajaran[1]}}">Matematika</option>
				<option @if($soal->kategori == $pelajaran[2]) selected="selected" @endif value="{{$pelajaran[2]}}">IPA</option>
			</select>

			<br><br>
			<label>Tahun Ajaran</label><br>
			<select name="tahun">
				<option @if($soal->tahun == "2015") selected="selected" @endif value="2015">2015</option>
				<option @if($soal->tahun == "2016") selected="selected" @endif value="2016">2016</option>
				<option @if($soal->tahun == "2017") selected="selected" @endif value="2017">2017</option>
				<option @if($soal->tahun == "2018") selected="selected" @endif value="2018">2018</option>
			</select>

			<br><br>
			<label>Soal</label>
			<textarea class="" name="soal" id="editor1" rows="8" cols="80" style="margin-bottom: 15px">{{ $soal->soal }}</textarea>

			<br>
			<label>Pilihan A</label>
			<textarea class="" name="pilihan_a" id="editor2" rows="8" cols="80" style="margin-bottom: 15px">{{ $soal->pilihan_a }}</textarea>

			<br>
			<label>Pilihan B</label>
			<textarea class="" name="pilihan_b" id="editor3" rows="8" cols="80" style="margin-bottom: 15px">{{ $soal->pilihan_b }}</textarea>

			<br>
			<label>Pilihan C</label>
			<textarea class="" name="pilihan_c" id="editor4" rows="8" cols="80" style="margin-bottom: 15px">{{ $soal->pilihan_c }}</textarea>

			<br>
			<label>Pilihan D</label>
			<textarea class="" name="pilihan_d" id="editor5" rows="8" cols="80" style="margin-bottom: 15px">{{ $soal->pilihan_d }}</textarea>

			<br>
			<label>Penjelasan</label>
			<textarea class="" name="penjelasan" id="editor6" rows="8" cols="80" style="margin-bottom: 15px">{{ $soal->penjelasan }}</textarea>

			<br>
			<label>Jawaban {{$soal->jawaban}}</label><br>
			<select name="jawaban">
				<option @if($soal->jawaban == "A") selected="selected" @endif value="A">A</option>
				<option @if($soal->jawaban == "B") selected="selected" @endif value="B">B</option>
				<option @if($soal->jawaban == "C") selected="selected" @endif value="C">C</option>
				<option @if($soal->jawaban == "D") selected="selected" @endif value="D">D</option>
			</select>

			<br><br>
			<label>Penjelasan dengan gambar</label> <br>
		    <input type="file" class="" id="" name="penjelasan_gambar">

			<br><br>
			<input type="submit" name="submit" value="Simpan">
			<input type="reset" name="clear" value="Reset">

		</form>
	</div>

	<div id="templatemo_sidebar">
		<a href="{{ url('/soal/create') }}">Buat Soal</a><br>
		<a href="{{ url('/soal') }}">Lihat Soal</a>
	</div>

	<div class="cleaner"></div>
</div>

<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
  CKEDITOR.replace( 'editor1' );
  CKEDITOR.replace( 'editor2' );
  CKEDITOR.replace( 'editor3' );
  CKEDITOR.replace( 'editor4' );
  CKEDITOR.replace( 'editor5' );
  CKEDITOR.replace( 'editor6' );
</script>


@endsection
