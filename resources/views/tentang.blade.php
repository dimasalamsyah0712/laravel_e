@extends('layouts.app')

@section('content')

<style>
  /* Create two equal columns that floats next to each other */
  .column {
      float: left;
      padding: 5px;
  }

  /* Clear floats after the columns */
  .row:after {
      content: "";
      display: table;
      clear: both;
  }
  h2, h4, h6{
    color: white;
  }

  .green{
    color: green;
    font-weight: bold;
  }
  .red{
    color: red;
    font-weight: bold;
  }

  .box{
    height: 250px;
    border-radius: 10px;
    background-color: #aae2da;
  }

  p{
    padding-left: 10px;
    padding-right: 10px;
  }

  h3 {
    padding: 10px;
    background-color: #4cbaaa;
    border-radius: 10px 10px 0px 0px;
  }

</style>



<div class="row">

  <div class="column" style="width: 48%">

    <div class="box">
      <h3>Title</h3>
      <p>Isinya</p>
    </div>

  </div>

  <div class="column" style="width: 48%; float: right;">
    <div class="box">
      <h3>Title</h3>
      <p>Isinya</p>
    </div>
  </div>

</div>

<div class="row">

  <div class="column" style="width: 48%">

    <div class="box">
      <h3>Title</h3>
      <p>Isinya</p>
    </div>

  </div>

  <div class="column" style="width: 48%; float: right;">
    <div class="box">
      <h3>Title</h3>
      <p>Isinya</p>
    </div>
  </div>

</div>

@endsection
