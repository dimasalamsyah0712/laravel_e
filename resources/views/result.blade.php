@extends('layouts.app')

@section('content')

<style>
	.quiz{
		background-color: white; 
		border-radius: 10px;
		padding: 10px;
	}
	/* Create two equal columns that floats next to each other */
	.column {
	    float: left;
	    width: 50%;
	}

	/* Clear floats after the columns */
	.row:after {
	    content: "";
	    display: table;
	    clear: both;
	}
	h2, h4, h6{
		color: white;
	}

	.green{
		color: green;
		font-weight: bold;
	}
	.red{
		color: red;
		font-weight: bold;
	}

</style>

<br><br>
<h2>Hasil</h2>
<h4>Ujian: {{$kategori}}</h4>
<h6>Tahun: {{$tahun}}</h6>


<form method="post" action="{{ route('ujian.store') }}">

@csrf
		@php $count = 0; @endphp


		@foreach($soals as $no=>$soal)

		<div class="quiz">

			<div class="row">
			  <div class="column" style="width: 3%;">
			  	<p>{{ ++$no }}</p>
			  </div>
			  <div class="column" style="width: 90%;">
			  	{!! $soal->soal !!}

			  	<input type="hidden" name="idSoal_{{$no}}" value="{{ $soal->id }}">



			  	<input @if($soal->jwbQuiz == "A") checked="checked" @endif value="A" type="radio" name="jwb_{{ $no }}" style="float: left; margin: 0; margin-right: 5px; margin-top: 3px;">
			  	<div @if($soal->jawaban == "A") class="green" @endif>
				  	<span style="float: left; margin: 0; margin-right: 5px;">A.</span>
					{!! $soal->pilihan_a !!}
				</div>

			  	<input @if($soal->jwbQuiz == "B") checked="checked" @endif value="B" type="radio" name="jwb_{{ $no }}" style="float: left; margin: 0; margin-right: 5px; margin-top: 3px;">
			  	<div @if($soal->jawaban == "B") class="green" @endif>
			  		<span style="float: left; margin: 0; margin-right: 5px;">B.</span>
					{!! $soal->pilihan_b !!}
				</div>

			  	<input @if($soal->jwbQuiz == "C") checked="checked" @endif value="C" type="radio" name="jwb_{{ $no }}" style="float: left; margin: 0; margin-right: 5px; margin-top: 3px;">
			  	<div @if($soal->jawaban == "C") class="green" @endif>
			  		<span style="float: left; margin: 0; margin-right: 5px;">C.</span>
					{!! $soal->pilihan_c !!}
				</div>

			  	<input @if($soal->jwbQuiz == "D") checked="checked" @endif value="D" type="radio" name="jwb_{{ $no }}" style="float: left; margin: 0; margin-right: 5px; margin-top: 3px;">
			  	<div @if($soal->jawaban == "D") class="green" @endif>
			  		<span style="float: left; margin: 0; margin-right: 5px;">D.</span>
					{!! $soal->pilihan_d !!}
				</div>

				Penjelasan:<br>
				{!! $soal->penjelasan !!}

				@if(empty($soal->penjelasan))
					<p>Penjelasan dengan gambar tidak tersedia</p>
				@else
					<img width="200px" src="{{ URL::asset('images/uploads/'.$soal->penjelasan_gambar ) }}">
				@endif

			  </div>
			</div>
			
			@php $count = $no; @endphp
			
		</div>

		<br>

		@endforeach

	@if($count == 0) 
		<br>
		<h3>Soal belum tersedia.</h3>
	@else
		<input type="hidden" name="count" value="{{ $count }}">
		<input type="hidden" name="kategori" value="{{ $kategori }}">
		<input type="hidden" name="tahun" value="{{ $tahun }}">
		
	@endif

	

</form>

<a style="color: white" href="{{url('/home')}}">Lihat score</a>

<br><br><br><br>


@endsection
