@extends('layouts.app')

@section('content')

<style>
	label{
		color: white;
	}
</style>

<img src="{{ URL::asset('images/uploads/'.Auth::user()->foto ) }}" style="width: 100px; float: right;">

  <h3>Ubah profile</h3>
  <form method="post" action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data"> {{-- {{ route('edit.update', $user->id) }} --}}
    @csrf
    @method('put')

    <label>Nama</label> <br>
    <input type="text" name="nama" value="{{Auth::user()->name}}">
    <br><br>

    <label>Tempat lahir</label> <br>
    <input type="text" name="tempat_lahir" value="{{Auth::user()->tempat_lahir}}">
    <br><br>

    <label>Tanggal lahir</label> <br>
    <input type="date" name="tgl_lahir" value="{{Auth::user()->tgl_lahir}}">
    <br><br>

    <label>Alamat</label> <br>
    <textarea name="alamat">{{Auth::user()->alamat}}</textarea>
    <br><br>

    <label>Foto</label> <br>
    <input style="color: white;" type="file" class="" id="" name="foto">
	<br><br>


    <button type="submit" name="button">Ubah</button>
  </form>

@endsection
