<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('vendor/templatemo_style.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/css/jquery.ennui.contentslider.css') }}" rel="stylesheet">
</head>
<body>

  @php
    $pelajaran= array(0 => 'B. Indonesia', 1 => 'Matematika', 2 => 'IPA'); 
    $pelajaranVal= array(0 => 'B. Indonesia', 1 => 'Matematika', 2 => 'IPA'); 
  @endphp

  <div id="templatemo_wrapper">

      <div id="templatemo_header">

        <div id="site_title">
              <h1><a href="#" target="_parent">
                  <img src="{{ asset('vendor/images/templatemo_logo.png') }}" alt="Site Title" width="200" height="50" />
                  <span>free website templates</span>
              </a></h1>
          </div>

          {{-- <div id="search_box">
              <form action="#" method="get">
                  <input type="text" value="Enter a keyword here..." name="q" size="10" id="searchfield" title="searchfield" onfocus="clearText(this)" onblur="clearText(this)" />
                  <input type="submit" name="Search" value="Search" alt="Search" id="searchbutton" title="Search" />
              </form>
          </div> --}}

          <div class="cleaner"></div>
      </div> <!-- end of header -->

      {{-- <div id="templatemo_menu">
          <ul>
              <li><a href="index.html" class="current">Home</a>
                <ul class="sub1">
                  <li><a href="#">sd</a></li>
                </ul>
              </li>
              <li><a href="services.html">Services</a></li>
              <li><a href="news.html">News</a></li>
              <li><a href="gallery.html">Gallery</a></li>
              <li><a href="contact.html">Contact Us</a></li>
          </ul>
      </div> --}}

      <div id='cssmenu'>
        <ul>
           <li><a href='{{url('/')}}'><span>Beranda</span></a></li>
           

           @guest

           @else
           {{-- <li><a href='{{url('/home')}}'><span>Nilai</span></a></li> --}}
               @if(Auth::user()->akses == "admin")

               <li class='has-sub'><a href="#"><span>Master Soal</span><i style="float: right;">&nabla;</i></a>
                  <ul>
                    <li class='has-sub'><a href='#'><span>{{ $pelajaran[0] }}</span> <i style="float: right;">&#8811</i></a>
                        <ul>
                           <li><a href='{{ url('soal/'.$pelajaran[0].'/2015') }}'><span>2015</span></a></li>
                           <li><a href='{{ url('soal/'.$pelajaran[0].'/2016') }}'><span>2016</span></a></li>
                           <li><a href='{{ url('soal/'.$pelajaran[0].'/2017') }}'><span>2017</span></a></li>
                        </ul>
                     </li>
                     <li class='has-sub'><a href='#'><span>MTK</span><i style="float: right;">&#8811</i></a>
                        <ul>
                           <li><a href='{{ url('soal/'.$pelajaran[1].'/2015') }}'><span>2015</span></a></li>
                           <li><a href='{{ url('soal/'.$pelajaran[1].'/2016') }}'><span>2016</span></a></li>
                           <li><a href='{{ url('soal/'.$pelajaran[1].'/2017') }}'><span>2017</span></a></li>
                        </ul>
                     </li>
                     <li class='has-sub'><a href='#'><span>IPA</span><i style="float: right;">&#8811</i></a>
                        <ul>
                           <li><a href='{{ url('soal/'.$pelajaran[2].'/2015') }}'><span>2015</span></a></li>
                           <li><a href='{{ url('soal/'.$pelajaran[2].'/2016') }}'><span>2016</span></a></li>
                           <li><a href='{{ url('soal/'.$pelajaran[2].'/2017') }}'><span>2017</span></a></li>
                        </ul>
                     </li>
                  </ul>
               </li>

               @endif



           <li class='has-sub'><a href="#"><span>Soal</span><i style="float: right;">&nabla;</i></a>
              <ul>
                <li class='has-sub'><a href='#'><span>{{ $pelajaran[0] }}</span> <i style="float: right;">&#8811</i></a>
                    <ul>
                       <li><a href='{{ url('ujian/'.$pelajaran[0].'/2015') }}'><span>2015</span></a></li>
                       <li><a href='{{ url('ujian/'.$pelajaran[0].'/2016') }}'><span>2016</span></a></li>
                       <li><a href='{{ url('ujian/'.$pelajaran[0].'/2017') }}'><span>2017</span></a></li>
                    </ul>
                 </li>
                 <li class='has-sub'><a href='#'><span>MTK</span><i style="float: right;">&#8811</i></a>
                    <ul>
                       <li><a href='{{ url('ujian/'.$pelajaran[1].'/2015') }}'><span>2015</span></a></li>
                       <li><a href='{{ url('ujian/'.$pelajaran[1].'/2016') }}'><span>2016</span></a></li>
                       <li><a href='{{ url('ujian/'.$pelajaran[1].'/2017') }}'><span>2017</span></a></li>
                    </ul>
                 </li>
                 <li class='has-sub'><a href='#'><span>IPA</span><i style="float: right;">&#8811</i></a>
                    <ul>
                       <li><a href='{{ url('ujian/'.$pelajaran[2].'/2015') }}'><span>2015</span></a></li>
                       <li><a href='{{ url('ujian/'.$pelajaran[2].'/2016') }}'><span>2016</span></a></li>
                       <li><a href='{{ url('ujian/'.$pelajaran[2].'/2017') }}'><span>2017</span></a></li>
                    </ul>
                 </li>
              </ul>
           </li>

           @endguest
           
           @guest
              <li class='last'><a href='{{ route('login') }}'><span>Masuk</span></a></li>
           @else
               <li class='has-sub'>
                  <a href="#"><span>Keluar</span></a>
                  <ul>
                    <li class="has-sub" >
                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ Auth::user()->name }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </li>
                    <li><a href="{{ route('user.edit', Auth::user()->id)}}"><span>Edit profile</span></a></li>
                  </ul>
               </li>
            @endguest
           <li class='last'><a href='{{ url('/tentang') }}'><span>Tentang</span></a></li>
            
        </ul>
      </div>

      @yield('content')

  </div>

    <!-- Scripts -->
    <script src="{{ asset('vendor/js/jquery-1.3.1.min.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.ennui.contentslider.js') }}"></script>
    <script type="text/javascript">
        $(function() {
        $('#one').ContentSlider({
        width : '600px',
        height : '240px',
        speed : 600,
        easing : 'easeInOutQuart'
        });
        });
    </script>
    {{-- <script src="{{ asset('vendor/js/jquery.chili-2.2.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/js/chili/recipes.js') }}"></script> --}}
</body>
</html>
