<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('vendor/templatemo_style.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/css/jquery.ennui.contentslider.css') }}" rel="stylesheet">
</head>
<body>

  <div id="templatemo_wrapper">

      <div id="templatemo_header">

      	<div id="site_title">
              <h1><a href="#" target="_parent">
                  <img src="{{ asset('vendor/images/templatemo_logo.png') }}" alt="Site Title" width="200" height="50" />
                  <span>free website templates</span>
              </a></h1>
        	</div>

          <div id="search_box">
              <form action="#" method="get">
                  <input type="text" value="Enter a keyword here..." name="q" size="10" id="searchfield" title="searchfield" onfocus="clearText(this)" onblur="clearText(this)" />
                  <input type="submit" name="Search" value="Search" alt="Search" id="searchbutton" title="Search" />
              </form>
          </div>

          <div class="cleaner"></div>
    	</div> <!-- end of header -->

      <div id="templatemo_menu">
          <ul>
              <li><a href="index.html" class="current">Home</a></li>
              <li><a href="services.html">Services</a></li>
              <li><a href="news.html">News</a></li>
              <li><a href="gallery.html">Gallery</a></li>
              <li><a href="contact.html">Contact Us</a></li>
          </ul>
    	</div> <!-- end of templatemo_menu -->

      <div id="templatemo_content_wrapper">
    		<span class="top"></span><span class="bottom"></span>
        <div id="templatemo_content">

            <div id="templatemo_slider">
              <div id="one" class="contentslider">
                  <div class="cs_wrapper">
                      <div class="cs_slider">

                        <div class="cs_article">
                            <a href="http://www.templatemo.com/page/1" target="_parent">
                                <img src="{{asset('vendor/images/slider/templatemo_slide02.jpg')}}" alt="Website Template 01" />
                            </a>
                        </div><!-- End cs_article -->

                        <div class="cs_article">
                            <a href="http://www.templatemo.com/page/1" target="_parent">
                                <img src="{{asset('vendor/images/slider/templatemo_slide02.jpg')}}" alt="Website Template 01" />
                            </a>
                        </div><!-- End cs_article -->

                        <div class="cs_article">
                            <a href="http://www.templatemo.com/page/1" target="_parent">
                                <img src="{{asset('vendor/images/slider/templatemo_slide02.jpg')}}" alt="Website Template 01" />
                            </a>
                        </div><!-- End cs_article -->

                      </div>
                  </div>
              </div>
            </div>

            <h2>Pro Teal Development</h2>

            <div class="service_box float_l">

                 <div class="service_image">
                    <img src="images/icon_01.png" alt="image 1" />
                 </div>
                 <div class="service_text">
                     <h5>Quality Services</h5>
                     <p>Donec varius tempor hendrerit. Nam convallis est ut lacus ullamcorper vitae scelerisque enim lobortis. Nam ut ipsum nec magna facilisis auctor ut. </p>
                     <div class="button"><a href="services.html">details</a></div>
                 </div>

            </div>

            <div class="service_box float_r">

                <div class="service_image">
                    <img src="images/icon_02.png" alt="image 2" />
                 </div>
                 <div class="service_text">
                     <h5>Best Products</h5>
                     <p>Nunc sed pharetra dui. Donec malesuada rutrum imperdiet. Etiam nec risus sit amet diam malesuada dictum non vitae est. Vivamus ac odio eros.</p>
                     <div class="button"><a href="services.html">details</a></div>
                 </div>

            </div>

            <div class="cleaner_h40"></div>

          <h2>What we do?</h2>
          <p>Pro Teal Template is a full-site (5 pages) <a href="http://www.templatemo.com" target="_parent">website template</a> provided by
            <a href="http://www.templatemo.com" target="_parent">templatemo.com</a> and you may edit and apply this template for your websites. Credits go to
            <a href="http://www.photovaco.com" target="_blank">Free Photos</a> for photos and <a href="http://www.smashingmagazine.com" target="_blank">SmashingMagazine.com</a> for icons used in this template. Validate
            <a  href="http://validator.w3.org/check?uri=referer" rel="nofollow">XHTML</a> <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow">CSS</a>.</p>
          <p>In ac libero urna. </p>


        </div> <!-- end -->

        <div id="templatemo_sidebar">

            <div class="section_rss_twitter">
                <div class="rss_twitter twitter">
                <a href="http://www.templatemo.com/page/1" target="_parent">FOLLOW US <span>on Twitter</span></a>
                </div>
                <div class="margin_bottom_20"></div>
                <div class="rss_twitter rss">
                <a href="http://www.templatemo.com/page/2" target="_parent">SUBSCRIBE <span>our feed</span></a>
                </div>
            </div>

            <div id="sidebar_featured_project">
                <h3>Featured Work</h3>
                <div class="left"><img src="images/maker.png" alt="image 3" /></div>
                <div class="right">
                    <a href="#">Lorem ipsum dolor sit</a>
                    <p>Cras purus libero, dapibus nec rutrum in, dapibus nec risus. Ut interdum mi sit amet magna feugiat auctor.</p>
                </div>
                <div class="cleaner"></div>
            </div>

            <div id="news_section">
                <h3>Promotion</h3>
                <div class="news_box">
                    <a href="#">Lorem ipsum dolor sit amet consectetur</a>
                    <p>Maecenas tellus erat, dictum vel semper a, dapibus ac elit. Nunc rutrum pretium porta.</p>
                </div>
                <div class="news_box">
                    <a href="#">Quisque id lacus in nunc porttitor</a>
                    <p>In fringilla, lorem in semper consectetur,  sapien est tempor ipsum, a convallis nisl neque in augue. </p>
                </div>
                <div class="button"><a href="news.html">View All</a></div>
				        <div class="cleaner"></div>
            </div>
            <div class="cleaner"></div>
        </div>

    	<div class="cleaner"></div>

      </div>

  </div>

    <!-- Scripts -->
    <script src="{{ asset('vendor/js/jquery-1.3.1.min.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.ennui.contentslider.js') }}"></script>
    <script type="text/javascript">
        $(function() {
        $('#one').ContentSlider({
        width : '600px',
        height : '240px',
        speed : 600,
        easing : 'easeInOutQuart'
        });
        });
    </script>
    {{-- <script src="{{ asset('vendor/js/jquery.chili-2.2.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/js/chili/recipes.js') }}"></script> --}}
</body>
</html>
