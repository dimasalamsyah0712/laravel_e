@extends('layouts.app')

@section('content')

<style>
	.box{
		height: 300px;
		float: left;
		background-color: white;
		border-radius: 20px;
		margin: 10px;
	}
	h2, h4{
		color: white;
	}
</style>


@guest

informasi web


@else

<br><br>
<h2>Score:</h2>

@if($soals == null)
	<h4>Belum ada soal yang dikerjakan.</h4>
@endif

@foreach($soals as $no=>$soal) 

	@if($soal->kategori == null)

		<h4>Belum ada soal yang dikerjakan.</h4>

	@else

		<div class="box" style="width: 31%">
			<center><h1 style="padding: 10px; margin-top: 20px;">{{$soal->kategori}}</h1>
				<h6>{{$soal->tahun}}</h6></center>
			<br><br><br>
			<center><span style="font-size: 100px; margin-top: 100px;">{{$soal->score}}</span></center>
		</div>

	@endif

	
@endforeach



@endguest



@endsection